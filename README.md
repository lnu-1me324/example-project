# Example Project

A running example that explores how to integrate the contents of the course in a simple project.

## re:Mind

I really like Apple's "Reminders" app, but I don't like the fact that it is only
for Apple users, and that there is no easy way to access it online. So, I decided
to stea... I mean, write a server-based app that is *inspired* by Reminders.

More information can be obtained in each file's comments.
