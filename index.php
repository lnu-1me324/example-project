<!doctype html>
<html lang=en>
<head>
  <meta charset=utf-8>
  <title>re:Mind</title>
  <style>
    .entry {
      font-family: sans-serif;
    }
    .late {
      color: red;
    }
    .on_time {
      color: gray;
    }
    .done {
      color: green;
    }
    .done_comment {
      font-weight: bold;
    }
  </style>
</head>
<body>
  <h1>re:Mind</h1>
  <?php
    # Code for reading NotYaml files
    require 'not_yaml.php';

    # Some helper functions

    # 'cmp' is used by 'usort' to compare entries and sort them from oldest to newest
    function cmp($a, $b) {
        return $a['date'] - $b['date'];
    }

    # 'span' simply echoes a class-formatted small amount of content
    function span($content, $class) {
      echo "<span class=$class>$content</span>";
    }

    # The 'entries' array is the result of reading the 'entries.yml' file
    $entries = readNotYaml('entries.yml');

    # Here we sort it from oldest (top) to newest (bottom)
    usort($entries, "cmp");

    # Start an unordered list for showing the entries
    echo '<ul>';
  
    # Loop that presents each entry from the 'entries' array
    foreach ($entries as $entry) {
      # The CSS class 'entry' formats the entire entry block
      echo "<li><p class=entry>";
      # Entries where 'done' == true should have a small comment before the text
      if (array_key_exists('done', $entry) && $entry['done']) {
        span('[DONE] ', 'done_comment');
      }
      # And finally show the actual text of the entry
      echo $entry['text'];
      echo '<br/>';
      # The timestamp of each entry is formatted according to whether it is 
      # late, ontime, or already done
      $timestamp = $entry['date'];
      if (array_key_exists('done', $entry) && $entry['done']) {
        $ts_class = 'done';
      } elseif ($timestamp < time()) {
        $ts_class = 'late';
      } else {
        $ts_class = 'on_time';
      }
      # Before showing the date, make sure it is human-readable
      span(date('Y-m-d H:i', $timestamp), $ts_class);

      echo '</p></li>';
    }

    echo '</ul>';
  ?>
</body>
</html>