<?php

/**
 * NotYaml is a file format that is definitely not Yaml. See 'entries.yml' for
 * an example. Note: 'text' and 'date' are mandatory; 'done' is assumed to be 
 * false if it is not present. The order of entries does not matter. 
 * 
 * The 'readNotYaml' function will read a NotYaml file and return its
 * contents as an array of associative arrays, each of which is an entry.
 */

function readNotYaml($filename) {
  # Don't proceed if the file does not exists, of course
  if (file_exists($filename)) {
    # Open file in 'read only' mode
    $file = fopen($filename, "r"); 
    # Start with an empty array of entries 
    $entries = array();
    # Read amd process each line of the file in sequence
    while ($line = fgets($file)) {
      # 'trim' will remove whitespace from the start and end of a string
      $line = trim($line);
      # If the first character of a line is a dash, that means a new entry
      if (strpos($line, '-') === 0) {
        # First of all save the current entry, if any
        if (isset($cur_entry)) {
            $entries[] = $cur_entry;
        }
        # Start a new entry (empty for now)
        $cur_entry = array();
        # Proceed with the line, without the dash
        $line = substr($line, 1);
      }
      # Split the line between key (before dash) and value (after dash)
      list($key, $value) = array_map(trim, explode(':', $line, 2));      
      # Depending on what the key is, the value will be processed differently
      switch (trim($key)) {
        # For text, simply save it without trailing or leading whitespace
        case 'text':
            $cur_entry['text'] = trim($value);
            break;
        # For date, first transform it into a generic timestamp, then save it
        case 'date':
            $timestamp = strtotime($value);
            $cur_entry['date'] = $timestamp;
            break;
        case 'done':
            $cur_entry['done'] = ($value == 'true'? true: false);
            break;
      }          
    }
    # Close the file as soon as we are finished using it
    fclose($file);
    # Save last entry (if any) before returning
    if (isset($cur_entry)) {
      $entries[] = $cur_entry;
    }
    return $entries;
  } else {
    // Don't tell the user what was the error, exactly. They don't need to know!
    echo "Error 123: Please contact the website's administrator.";
  }
}
?>